myDrop n xs = if n <= 0 || null xs 
               then xs
               else myDrop (n-1) (tail xs)

lastButOne xs =  head (drop 1 (reverse xs))


-- reformulation that uses patterns and guards 
niceDrop n xs | n <= 0 = xs 
niceDrop _ []          = []
niceDRop n (_:xs)      = niceDrop (n - 1) xs