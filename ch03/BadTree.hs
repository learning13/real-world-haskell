data Tree a = Node a (Tree a) (Tree a) 
          | Empty
            deriving (Show)


-- bad_nodesAreSame (Node a _ _ ) (Node a _ ) = Just a -- name can't appear in multiple places
-- bad_nodesAreSame _             _          = Nothing

nodesAreSame :: Eq a => Tree a -> Tree a -> Maybe a
nodesAreSame (Node a _ _ ) (Node b _ _) 
     | a == b = Just a 
nodesAreSame _ _ = Nothing

-- a pattern can be followed by zero or more guards

height:: Tree a -> Integer 
height Empty = 0
height (Node _ (left) (right)) = 
     case (left , right) of 
          (Empty, Empty) -> 1
          (Empty, _ ) -> 1 + height right 
          (_, Empty) -> 1 + height left
          (_, _)  -> 1 + max (height left) (height right)

