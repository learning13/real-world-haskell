import Data.List

myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs


calculateMean :: [Integer] -> Double
calculateMean [] = 0.0 
calculateMean xs = 
     let a = fromIntegral (sum xs)
         b = fromIntegral (length xs)
     in  a / b 


     
createPalindromeEasy :: [a] -> [a]
createPalindromeEasy xs = xs ++ reverse xs 

-- palindrome without using reverse p
createPalindromeHard :: [a] -> [a]
createPalindromeHard [] =  []
createPalindromeHard xs = createPhelper xs xs

createPhelper :: [a] -> [a] -> [a]
createPhelper xs [] = xs
createPhelper xs ys = 
     let num = (length ys) - 1
         xs' = xs ++ drop num ys
         ys' = take num ys
     in createPhelper xs' ys'
         
isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome xs = 
     let xs' = drop 1 xs
         xs'' = take ((length xs') - 1) xs'
     in 
     (head xs) == (last xs) && isPalindrome(xs'')

sortSubLists :: [[a]] -> [[a]]
sortSubLists list = sortBy (\ xs  ys -> compare (length xs) (length ys)) list