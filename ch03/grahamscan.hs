import Data.Ratio


data Direction = Left | Straight | Right

type Point = (Integer, Integer)

calculateSlope :: Point -> Point -> Rational
calculateSlope (x0,y0) (x1,y1) | x0 /= x1  =  (y1 - y0) % (x1 - x0)
calculateSlope (_,_)  (_,_) = 0


