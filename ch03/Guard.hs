fromMaybe :: p -> Maybe p -> p
fromMaybe defval wrapped = 
     case wrapped of 
          Nothing -> defval
          Just value -> value
