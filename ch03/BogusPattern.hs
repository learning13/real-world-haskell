-- The following are some attempts at pattern matching gone awry.

data Fruit = Apple | Orange | Other
            deriving (Show)

apple = "apple"

orange = "orange"

whichFruit :: String -> Fruit
whichFruit f = case f of 
          apple -> Apple  -- apple does not refer to the top level value named apple it is a local pattern variable
          orange -> Orange


-- the following are irrefutable patterns because they always match/succeed. Plain variable names and the wildcard _ are examples of irrefutable patterns
equational apple = Apple
equational orange = Orange


betterFruit f = case f of
               "orange" -> Orange
               "apple" -> Apple
               _ -> Other
