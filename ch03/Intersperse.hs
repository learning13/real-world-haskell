
intersperse :: a-> [[a]] -> [a]
intersperse _ [] = [] 
intersperse _ (y:[]) = y
intersperse x (y:ys) =  
     y ++ [x] ++ intersperse x ys
