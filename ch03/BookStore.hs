data BookInfo = Book Int String [String]
                deriving (Show)
data MagazineInfo = Magazine Int String [String]
                    deriving (Show)

myInfo = Book 9781035072455 "Algebra of Programming" ["Richard Bird", "Oege de Moor"]

data BookReview = BookReview BookInfo CustomerID String

type CustomerID = Int 
type ReviewBody = String 

data BetterReview = BetterReview BookInfo CustomerID ReviewBody



