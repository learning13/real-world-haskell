foldl :: (a -> b -> a) -> a -> [b] -> a

foldl step zero (x:xs) = Main.foldl step (step zero x) xs
foldl _    zero [] = zero

foldSum xs = Main.foldl step 0 xs
     where step acc x = acc + x

niceSum :: [Integer] -> Integer
niceSum xs = Main.foldl (+) 0 xs

foldr :: ( a -> b -> b) -> b -> [a] -> b
foldr step zero (x:xs) = step x (foldr step zero xs)
foldr _ zero [] = zero

