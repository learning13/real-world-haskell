niceSum :: [Integer] -> Integer 
niceSum xs = foldl (+) 0 xs

-- we can omit the xs from both the parameter list and the parameters
-- to fold 

nicerSum :: [Integer] -> Integer
nicerSum = foldl (+) 0 

