safeHead (x: _) = Just x
safeHead _ = Nothing

unsafeHead = \(x:_) -> x 
-- unsafeHead will error out at runtime aka Non-exhaustive patterns in lambda

