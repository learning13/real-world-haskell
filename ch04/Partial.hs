isInAny needle haystack = any inSequence haystack
     where inSquence s = needle `isInfixOf` s

-- rewrite using lambda function

isInAny2 needle haystack = any (\s -> needle `isInfixOf` s) haystack

-- lambda can only have a single clause in its definition 

isInAny3 needle haystack = any (isInfixOs needle) haystack
-- isInfixOf needle is a partially applied function

-- partial function application is named currying, after the logician 
-- Haskell Curry (for whom the Haskell Language is named)

isInAny4 needle haystack = any (needle `isInfixOf`) haystack