oddList :: [Int] -> [Int]
oddList (x:xs) | odd x     = x : oddList xs
               | otherwise = oddList xs
oddList _                  = []

filter odd [3,1,4,1,5,9,6,5] 
     = oddList [3,1,4,1,5,9,6,5]