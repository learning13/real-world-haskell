import Data.Char (digitToInt,isDigit)
import Data.List (foldr, foldl, foldl')

safeSingle :: ([a] -> a) -> ([a] -> Maybe a)
safeSingle function [] = Nothing 
safeSingle function xs = Just (function xs)

safeMany :: ([a] -> [a]) -> ([a] -> Maybe [a])
safeMany function [] = Nothing 
safeMany function xs = Just (function xs)


safeHead :: [a] -> Maybe a 
safeHead [] = Nothing
safeHead (x:xs) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail xs = Just (tail xs)

safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast xs = Just (last xs)

safeInit :: [a] -> Maybe [a]
safeInit = safeMany init

asInt_fold :: String -> Int
asInt_fold "" = error "empty String"
asInt_fold "-" = error "not a digit"
asInt_fold ('-':xs) = -1 * asInt_fold xs
asInt_fold xs = foldl step 0 xs 
        where  step acc char | isDigit char = digitToInt char + acc * 10
               step acc char = error "encountered non digit"

type ErrorMessage = String

asInt_foldr :: String -> Either ErrorMessage Int
asInt_foldr ('-':xs) = 
     case asInt_foldr xs of 
          Left message -> Left message
          Right num -> Right (-1 * num)
asInt_foldr xs = snd (helper xs)

helper :: String -> (Int, Either ErrorMessage Int)
helper xs = foldr step (0, Right 0) xs
  where
    step char (pos, accum) =
      case (accum, isDigit char) of
        ((Left message), _) -> (pos, accum)
        ((Right _), False) -> (pos, (Left ("non digit " ++ [char]) ))
        (Right num, True) -> (pos + 1, Right (10 ^ pos * (digitToInt char) + num)) 