myDumbExample xs = if length xs > 0
                   then head xs
                   else 'z'

-- length xs will walk the entire list 
-- if the list is infinite it will cause an infinite loop

mySmartExample xs = if not (null xs)
                    then head xs
                    else 'Z'
                    