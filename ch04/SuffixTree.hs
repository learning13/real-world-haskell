import Data.List (tails)

suffixes :: [a] -> [[a]]
suffixes xs@(_:xs') = xs : suffixes xs'
suffixes _ = []

noAsPattern :: [a] -> [[a]]
noAsPattern (x:xs) = (x:xs) : noAsPattern xs
noAsPattern _ = []

suffixes2 xs = init (tails xs)

-- init returns all but the last element of a list 

compose :: (b -> c) -> (a -> b) -> a -> c
compose f g x = f (g x)

suffixes3 xs = compose init tails xs 

-- point free style, can drop the point (aka paramater)
suffixes4 :: [a] -> [[a]]
suffixes4 = compose init tails

-- using the Prelude function . 
suffixes5 = init . tails
