import Data.Char (toUpper)

upcaseFirst (c:cs) = toUpper c -- forget ":cs" here
-- compiler will infer its type as String -> Char

camelCase :: String -> String
camelCase xs = concat (map upcaseFirst (words xs))

