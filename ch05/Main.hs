module Main where 

import SimpleJSON

-- to create an executable ghc expects a module named Main that contains a function named main

main = print (JObject [("foo", JNumber 1), ("bar", JBool False)])